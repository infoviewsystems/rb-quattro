package com.rb.mule.quattro.utils;
import java.io.File;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

public class DeleteFile implements Callable {

	public Object onCall(MuleEventContext eventContext) throws Exception {
		
		String filePath = eventContext.getMessage().getInboundProperty("originalDirectory");
		String fileName = eventContext.getMessage().getInboundProperty("originalFilename");
		String fileFullPath = filePath + File.separator + fileName;
		
		File file = new File(fileFullPath);
		   boolean isDeleted = false;
		   isDeleted = java.nio.file.Files.deleteIfExists(file.toPath());
		   if (file.isFile()) {
		       isDeleted = file.delete();
		   }
		   
	 return isDeleted;
	}
}
